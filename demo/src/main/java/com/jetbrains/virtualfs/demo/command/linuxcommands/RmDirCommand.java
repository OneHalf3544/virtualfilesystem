package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.VFile;

import java.io.IOException;
import java.util.List;

public class RmDirCommand extends Command {

    public RmDirCommand() {
        super("rmdir");
    }

    @Override
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) throws IOException {
        VFile file = getArgumentAsDirectory(arguments, 0, currentDir);
        fileSystem.remove(file);
        return "file '" + file + "'removed";
    }
}
