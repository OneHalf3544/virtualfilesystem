package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.VFile;
import lombok.SneakyThrows;

import java.util.List;

public class CpCommand extends Command {

    public CpCommand() {
        super("cp");
    }

    @Override
    @SneakyThrows
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) {
        VFile source = getArgumentAsFile(arguments, 0, currentDir);
        VFile destination = getArgumentAsFile(arguments, 1, currentDir);
        fileSystem.writeFile(destination, fileSystem.readFromFile(source));
        return "copying " + source + " to " + destination;
    }
}
