package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.Directory;
import com.jetbrains.virtualfs.files.VFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class CdCommand extends Command {

    public CdCommand() {
        super("cd");
    }


    @Override
    public Pair<String, VFile> execute(FileSystem fileSystem, VFile currentDir, List<String> arguments) {
        if (arguments.size() != 1) {
            return Pair.of("one argument expected", currentDir);
        }

        String newDirectory = arguments.get(0);
        if (newDirectory.equals("..")) {
            newDirectory = currentDir.isRoot() ? "/" : currentDir.getParent().getName();
        }
        if (!newDirectory.startsWith("/")) {
            newDirectory = currentDir.getName() + newDirectory;
        }
        if (!newDirectory.endsWith("/")) {
            newDirectory = newDirectory + "/";
        }

        Optional<String> error = validatePath(fileSystem, newDirectory);
        if (error.isPresent()) {
            return Pair.of(error.get(), currentDir);
        }

        return Pair.of(
                "set directory '" + newDirectory + "' as current",
                new VFile(newDirectory));
    }

    private Optional<String> validatePath(FileSystem fileSystem, String newDirectory) {
        try {

            fileSystem.loadDirectory(new VFile(newDirectory));
            return Optional.empty();

        } catch (Throwable throwable) {
            log.info("error", throwable);
            return Optional.of(throwable.getClass().getSimpleName() + " error: '" + throwable.getMessage() + "'");
        }
    }

    @Override
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) {
        throw new UnsupportedOperationException();
    }
}
