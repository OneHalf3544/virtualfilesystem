package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.VFile;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class CatCommand extends Command {

    public CatCommand() {
        super("cat");
    }

    @Override
    @SneakyThrows
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) {
        VFile file = getArgumentAsFile(arguments, 0, currentDir);
        if (file.isDirectory()) {
            throw new IllegalArgumentException(file + " is a directory");
        }
        return IOUtils.toString(fileSystem.readFromFile(file), StandardCharsets.UTF_8.name());
    }
}
