package com.jetbrains.virtualfs.demo.command;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.files.VFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class ImportCommand extends Command {

    public ImportCommand() {
        super("import");
    }

    @Override
    protected Optional<String> validateArguments(List<String> arguments) {
        if (arguments.isEmpty()) {
            return Optional.of("expected one argument with external operation system file name");
        }
        return Optional.empty();
    }

    @Override
    protected String doExecute(FileSystem fileSystem,
                               VFile currentDir,
                               List<String> arguments) throws IOException {
        File externalFile = new File(arguments.get(0));
        fileSystem.importFile(externalFile, currentDir);

        return String.format("%s imported to %s", externalFile, currentDir);
    }
}
