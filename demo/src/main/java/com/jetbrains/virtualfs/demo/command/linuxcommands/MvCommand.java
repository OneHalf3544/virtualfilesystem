package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.VFile;

import java.io.IOException;
import java.util.List;

public class MvCommand extends Command {

    public MvCommand() {
        super("mv");
    }

    @Override
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) throws IOException {
        VFile oldName = getArgumentAsFile(arguments, 0, currentDir);
        VFile newName = getArgumentAsFile(arguments, 1, currentDir);
        fileSystem.rename(oldName, newName);
        return "file " + oldName + " renamed to " + newName;
    }
}
