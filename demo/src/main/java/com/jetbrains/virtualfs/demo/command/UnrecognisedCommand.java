package com.jetbrains.virtualfs.demo.command;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.DemoApplication;
import com.jetbrains.virtualfs.files.VFile;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

public class UnrecognisedCommand extends Command {

    public UnrecognisedCommand() {
        super("unrecognised");
    }

    @Override
    public String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) {
        return "unknown command. you can use only one of " + DemoApplication.COMMANDS.keySet();
    }
}
