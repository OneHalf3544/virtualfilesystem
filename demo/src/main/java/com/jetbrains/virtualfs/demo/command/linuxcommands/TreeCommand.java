package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.Directory;
import com.jetbrains.virtualfs.files.VFile;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class TreeCommand extends Command {

    public TreeCommand() {
        super("tree");
    }

    @Override
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) {
        VFile treeDir = arguments.size() == 0 ? currentDir : getArgumentAsDirectory(arguments, 0, currentDir);
        List<String> allFiles = getAllFiles(fileSystem, treeDir);
        return toTreeRepresentation(allFiles, treeDir.getName());
    }

    String toTreeRepresentation(List<String> allFiles, String prefixToRemove) {
        int currentDirNameLength = prefixToRemove.length() - 1;
        return allFiles.stream()
                .map(file -> file.substring(currentDirNameLength))
                .map(name -> name
                        .replaceAll("/[^/]+(?=/(?!$))", "-")
                        .replaceFirst("^(-*)-(.*)$", "$1|-$2")
                        .replaceAll("-(?=[\\-|])",  " ")
                )
                .map(Object::toString)
                .collect(joining("\n"));
    }

    @SneakyThrows
    private List<String> getAllFiles(FileSystem fileSystem, VFile directoryName) {
        Directory directory = fileSystem.loadDirectory(directoryName);
        return directory.getNames().stream()
                .flatMap(name -> Stream.concat(
                        Stream.of(directoryName + name),
                        isDirName(name)
                                ? getAllFiles(fileSystem, new VFile(directoryName + name)).stream()
                                : Stream.empty()))
                .collect(Collectors.toList());
    }

    private boolean isDirName(String name) {
        return name.endsWith("/");
    }
}
