package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.VFile;
import lombok.SneakyThrows;

import java.util.List;

public class RmCommand extends Command {

    public RmCommand() {
        super("rm");
    }

    @Override
    @SneakyThrows
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) {
        VFile file = getArgumentAsFile(arguments, 0, currentDir);
        fileSystem.remove(file);
        return "deleting file " + file;
    }
}
