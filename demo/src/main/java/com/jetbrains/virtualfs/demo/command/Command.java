package com.jetbrains.virtualfs.demo.command;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.files.VFile;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@ToString
@Getter
@AllArgsConstructor
@Slf4j
public abstract class Command {

    private final String name;

    /**
     * An entry point for the command execution.
     * Validates input argument by rules in an overridden method {@link #validateArguments(List)}
     * This method handles any exceptions that occurs in custom command code {@link #doExecute(FileSystem, VFile, List)}).
     *
     * @param fileSystem virtual file system on which we make operation
     * @param currentDir current virtual directory from demo shell
     * @param arguments command arguments
     * @return pair of result string representation and new current directory for shell.
     */
    public Pair<String, VFile> execute(@NonNull FileSystem fileSystem,
                                       @NonNull VFile currentDir,
                                       @NonNull List<String> arguments) {
        Optional<String> errors = validateArguments(arguments);
        if (errors.isPresent()) {
            return Pair.of(errors.get(), currentDir);
        }
        try {
            return Pair.of(doExecute(fileSystem, currentDir, arguments), currentDir);
        } catch (Throwable t) {
            log.info("error", t);
            return Pair.of(t.getClass().getSimpleName() + " error: '" + t.getMessage() + "'", currentDir);
        }
    }

    protected Optional<String> validateArguments(List<String> arguments) {
        return Optional.empty();
    }

    protected abstract String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) throws IOException;

    protected VFile getArgumentAsDirectory(List<String> arguments, int i, VFile currentDir) {
        VFile file = getArgumentAsFile(arguments, i, currentDir);
        return file.isDirectory() ? file : new VFile(file.getName() + "/");
    }

    protected VFile getArgumentAsFile(List<String> arguments, int i, VFile currentDir) {
        if (arguments.size() <= i) {
            throw new IllegalArgumentException("an argument expected at " + (i + 1) + " position");
        }
        String name = arguments.get(i);
        if (!name.startsWith("/")) {
            name = currentDir + name;
        }
        return new VFile(name);
    }
}
