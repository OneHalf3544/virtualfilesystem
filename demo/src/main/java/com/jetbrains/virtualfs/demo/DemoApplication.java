package com.jetbrains.virtualfs.demo;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.FileSystemFactory;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.demo.command.EditCommand;
import com.jetbrains.virtualfs.demo.command.ExportCommand;
import com.jetbrains.virtualfs.demo.command.ImportCommand;
import com.jetbrains.virtualfs.demo.command.StatsCommand;
import com.jetbrains.virtualfs.demo.command.UnrecognisedCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.CatCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.CdCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.CpCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.LsCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.MkDirCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.MvCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.RmCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.RmDirCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.TouchCommand;
import com.jetbrains.virtualfs.demo.command.linuxcommands.TreeCommand;
import com.jetbrains.virtualfs.files.VFile;
import com.jetbrains.virtualfs.statistic.Statistic;
import lombok.SneakyThrows;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;

import static com.jetbrains.virtualfs.FileSystemFactory.createFileSystem;
import static com.jetbrains.virtualfs.FileSystemFactory.loadFileSystem;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

public class DemoApplication {

    public static final Map<String, Command> COMMANDS = List.of(
            new CatCommand(),
            new CdCommand(),
            new CpCommand(),
            new EditCommand(),
            new ExportCommand(),
            new ImportCommand(),
            new LsCommand(),
            new MkDirCommand(),
            new MvCommand(),
            new RmCommand(),
            new RmDirCommand(),
            new StatsCommand(),
            new TreeCommand(),
            new TouchCommand())
            .stream()
            .collect(toMap(Command::getName, Function.identity()));

    private static final Command UNRECOGNISED_COMMAND = new UnrecognisedCommand();

    private static final String userName = System.getProperty("user.name");
    private static final String computer = getComputerName();

    @SneakyThrows
    private static String getComputerName() {
        return InetAddress.getLocalHost().getHostName();
    }

    /**
     * Demo application entry point.
     * @param args arguments used to run application.
     *             Only first argument will be used if the array is not empty.
     *             It will be considered as a name of a real file that should be loaded as content of virtual file system
     *             (if that file doesn't exists, it will be created with the empty content).
     * @throws IOException in case of io errors
     */
    public static void main(String... args) throws IOException {
        print("file system demo");
        print("you can use commands: " + COMMANDS.keySet().stream().sorted().collect(joining(", ")));
        print("type 'exit' to terminate the application\n");

        try (FileSystem fileSystem = createOrLoadFileSystem(args)) {
            // hook for debug purpose (don't spoil the file as result of force restart in IDEA):
            Runtime.getRuntime().addShutdownHook(closeFile(fileSystem));

            VFile currentDir = new VFile("/");

            Scanner scanner = new Scanner(System.in);

            Statistic statistic = fileSystem.getStatistics();
            while (true) {
                System.out.print(String.format("%s@%s:%s& ", userName, computer, currentDir));
                String line = scanner.nextLine();
                String[] split = line.split(" ");
                String commandName = split[0];
                List<String> arguments = getArguments(split);
                if ("exit".equals(commandName)) {
                    break;
                }
                Command command = COMMANDS.getOrDefault(commandName, UNRECOGNISED_COMMAND);
                Pair<String, VFile> result = command.execute(fileSystem, currentDir, arguments);
                print(result.getLeft());
                print("");
                currentDir = result.getRight();

                statistic = printWarningsIfErrorFound(fileSystem, statistic);
            }
        }
    }

    private static Thread closeFile(FileSystem fileSystem) {
        return new Thread(() -> {
            try {
                fileSystem.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private static Statistic printWarningsIfErrorFound(FileSystem fileSystem, Statistic oldStatistic) {
        Statistic newStatistic = fileSystem.getStatistics();

        if (newStatistic.getLostSpaceCount() > oldStatistic.getLostSpaceCount()) {
            print(String.format(
                    "\nWARNING: lost blocks count increased from %d to %d\n\n",
                    oldStatistic.getLostSpaceCount(),
                    newStatistic.getLostSpaceCount()));
        }

        return newStatistic;
    }

    private static FileSystem createOrLoadFileSystem(String[] args) throws IOException {
        FileSystem fileSystem;
        if (args.length == 0) {
            fileSystem = createTemporaryFileSystem();
        } else {
            File file = new File(args[0]);
            print("use file " + file.getAbsoluteFile());
            fileSystem = file.exists() ? loadFileSystem(file) : createFileSystem(file);
        }
        return fileSystem;
    }

    private static FileSystem createTemporaryFileSystem() throws IOException {
        File tempFile = File.createTempFile("virtualfs-", ".vfs");
        tempFile.deleteOnExit();

        print("create file system in file " + tempFile);
        return FileSystemFactory.createFileSystem(tempFile);
    }

    private static List<String> getArguments(String[] split) {
        if (split.length <= 1) {
            return Collections.emptyList();
        }
        return Arrays.asList(split).subList(1, split.length);
    }

    private static void print(String string) {
        System.out.println(string);
    }

}