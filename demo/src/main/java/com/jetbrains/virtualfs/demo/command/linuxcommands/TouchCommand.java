package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.VFile;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.List;

public class TouchCommand extends Command {

    public TouchCommand() {
        super("touch");
    }

    @Override
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) throws IOException {
        VFile file = getArgumentAsFile(arguments, 0, currentDir);
        fileSystem.writeFile(file, IOUtils.toInputStream(""));
        return file + " created";
    }
}
