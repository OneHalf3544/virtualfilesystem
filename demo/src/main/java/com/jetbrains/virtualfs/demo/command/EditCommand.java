package com.jetbrains.virtualfs.demo.command;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.files.VFile;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * A command for changing a file content.
 * All user input will be written to file until 'EOF' string appears.
 */
public class EditCommand extends Command {

    public EditCommand() {
        super("edit");
    }

    @Override
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) throws IOException {
        VFile file = getArgumentAsFile(arguments, 0, currentDir);
        System.out.println("Input file content. Type 'EOF' as the last line.");

        fileSystem.writeFile(file, collectInput());

        return "new content written into file " + file;
    }

    private InputStream collectInput() throws IOException {
        Scanner scanner = new Scanner(System.in, chooseEncodingToReadFromConsole());
        StringBuilder fileContent = new StringBuilder();
        while (true) {
            String line = scanner.nextLine();
            if ("EOF".equals(line)) {
                break;
            }
            fileContent.append(line).append('\n');
        }
        removeLastLineBreak(fileContent);

        return IOUtils.toInputStream(fileContent.toString(), StandardCharsets.UTF_8.name());
    }

    private String chooseEncodingToReadFromConsole() {
        Charset charset = Charset.defaultCharset();
        // hack for windows:
        if (Objects.equals(charset.name(), "windows-1251")) {
            return "IBM866";
        }
        return charset.name();
    }

    private void removeLastLineBreak(StringBuilder fileContent) {
        if (fileContent.length() != 0) {
            fileContent.setLength(fileContent.length() - 1);
        }
    }
}
