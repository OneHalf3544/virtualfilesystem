package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.Directory;
import com.jetbrains.virtualfs.files.VFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class LsCommand extends Command {

    public LsCommand() {
        super("ls");
    }

    @Override
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) throws IOException {
        VFile lsDir = arguments.size() == 0 ? currentDir : getArgumentAsDirectory(arguments, 0, currentDir);
        Directory directory = fileSystem.loadDirectory(lsDir);
        return "content of directory '" + lsDir + "':\n"
                + directory.getNames().stream().collect(Collectors.joining("\n"));
    }
}
