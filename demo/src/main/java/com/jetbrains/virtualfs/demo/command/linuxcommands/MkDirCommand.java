package com.jetbrains.virtualfs.demo.command.linuxcommands;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.demo.command.Command;
import com.jetbrains.virtualfs.files.VFile;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class MkDirCommand extends Command {

    public MkDirCommand() {
        super("mkdir");
    }

    @Override
    protected Optional<String> validateArguments(List<String> arguments) {
        if (arguments.size() != 1) {
            return Optional.of("one argument expected");
        }
        return Optional.empty();
    }

    @Override
    public String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) throws IOException {
        String newDirName = arguments.get(0);
        fileSystem.createDirectory(currentDir, newDirName);
        return "created '" + newDirName + "'";
    }
}
