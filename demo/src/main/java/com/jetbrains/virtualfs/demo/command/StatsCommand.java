package com.jetbrains.virtualfs.demo.command;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.files.VFile;
import com.jetbrains.virtualfs.statistic.StatCollector;

import java.io.IOException;
import java.util.List;

public class StatsCommand extends Command {

    private final StatCollector statCollector = new StatCollector();

    public StatsCommand() {
        super("stat");
    }

    @Override
    protected String doExecute(FileSystem fileSystem, VFile currentDir, List<String> arguments) throws IOException {
        return statCollector.analyse(fileSystem).toString();
    }
}
