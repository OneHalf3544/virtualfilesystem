package com.jetbrains.virtualfs.demo.command.linuxcommands

import spock.lang.Specification

class TreeCommandTest extends Specification {

    def 'should print list of files as tree'() {
        given: "list of files"
        def filesList = """
                /sources/
                /sources/virtualfs/
                /sources/virtualfs/out/
                /sources/virtualfs/out/production/
                /sources/virtualfs/out/production/classes/
                /sources/virtualfs/out/production/classes/com/
                /sources/virtualfs/out/production/classes/com/jetbrains/
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/files/
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/files/FsInputStream.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/files/Directory_1.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/files/Directory.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/files/FsOutputStream.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/files/VFile.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/blocks/
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/blocks/BlockWriter.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/blocks/Block.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/blocks/BlockReader.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/blocks/FileSystemFirstBlock.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/blocks/Pointer.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/FileSystemImpl.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/FileSystem.class
                /sources/virtualfs/out/production/classes/com/jetbrains/virtualfs/FileSystemFactory.class
                """.stripIndent()
                .readLines()
                .findAll {!it.isEmpty()}

        when: "format"
        def result = new TreeCommand().toTreeRepresentation(filesList, '/sources/')

        then:
        result == """|/
                 |/virtualfs/
                 ||-/out/
                 | |-/production/
                 |  |-/classes/
                 |   |-/com/
                 |    |-/jetbrains/
                 |     |-/virtualfs/
                 |      |-/files/
                 |       |-/FsInputStream.class
                 |       |-/Directory_1.class
                 |       |-/Directory.class
                 |       |-/FsOutputStream.class
                 |       |-/VFile.class
                 |      |-/blocks/
                 |       |-/BlockWriter.class
                 |       |-/Block.class
                 |       |-/BlockReader.class
                 |       |-/FileSystemFirstBlock.class
                 |       |-/Pointer.class
                 |      |-/FileSystemImpl.class
                 |      |-/FileSystem.class
                 |      |-/FileSystemFactory.class""".stripMargin()
    }
}
