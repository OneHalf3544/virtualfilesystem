package com.jetbrains.virtualfs;

import com.jetbrains.virtualfs.files.Directory;
import com.jetbrains.virtualfs.files.VFile;
import com.jetbrains.virtualfs.statistic.Statistic;
import lombok.NonNull;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * A virtual file system interface.
 */
public interface FileSystem extends Closeable {

    /**
     * Copy external file or directory into inner FS.
     *
     * @param outerFile a file to copy
     * @param innerFile a destination
     * @throws IOException in case of IO errors
     */
    void importFile(File outerFile, VFile innerFile) throws IOException;

    /**
     * Copy an internal file or directory into outer FS.
     *
     * @param innerFile a file to copy
     * @param outerFile a destination
     * @throws IOException in case of IO errors
     */
    void exportFile(VFile innerFile, File outerFile) throws IOException;

    /**
     * Creates file.
     *
     * @param file name of a created file
     * @param inputStream content of the file
     * @return name of created file
     * @throws IOException in case of i/o failures
     */
    VFile writeFile(VFile file, InputStream inputStream) throws IOException;

    /**
     * Check if the file or the directory exists.
     * @param file name of the checked file
     * @return true if file exists
     * @throws IOException in case of i/o failures
     */
    boolean exists(VFile file) throws IOException;

    /**
     * Reads content of specified file.
     *
     * @param file name of the file
     * @return stream of file content
     * @throws IOException in case of i/o failures
     */
    InputStream readFromFile(VFile file) throws IOException;

    /**
     * Delete file content.
     *
     * @param file name of deleted file
     * @throws IOException in case of i/o failures
     */
    void remove(VFile file) throws IOException;

    /**
     * Create a directory.
     *
     * @param parent a parent directory
     * @param directoryName a name of new directory
     * @return an absolute name of created
     * @throws IOException in case of i/o failures
     */
    VFile createDirectory(VFile parent, @NonNull String directoryName) throws IOException;

    /**
     * Load a diretory info.
     *
     * @param directory an absolute name of the directory
     * @return directory content
     * @throws IOException in case of IO error
     */
    Directory loadDirectory(VFile directory) throws IOException;

    /**
     * Rename (move) a file or a directory.
     *
     * @param oldName an old name
     * @param newName a new name. It can point to a file in another directory
     *                if you need to move file to another location.
     * @throws IOException in case of i/o failures
     */
    void rename(VFile oldName, VFile newName) throws IOException;

    /**
     * Get an info about file system. Number of used blocks, lost blocks, files, directories, etc.
     *
     * @return collected info
     */
    Statistic getStatistics();


    /**
     * Get the file that contains this virtual filesystem.
     *
     * @return the real file of FS
     */
    File getSystemFile();

    /**
     * Close the system file.
     *
     * @throws IOException in case of i/o failures
     */
    void close() throws IOException;
}
