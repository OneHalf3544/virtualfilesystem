package com.jetbrains.virtualfs.blocks;

import com.jetbrains.virtualfs.FileSystemImpl;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.RandomAccessFile;

import static com.jetbrains.virtualfs.FileSystemImpl.assertPointerRange;

@Slf4j
public class BlockReader {

    /**
     * Reads a block content addressed by {@code startPoint}.
     *
     * @param startPoint a pointer to the block
     * @param randomAccessFile {@link RandomAccessFile} to access the file content.
     * @return the read block
     * @throws IOException in case of IO errors
     */
    public static Block readBlock(@NonNull Pointer startPoint,
                                  @NonNull RandomAccessFile randomAccessFile) throws IOException {
        log.debug("read block at pointer {}", startPoint);
        assertPointerRange(startPoint, randomAccessFile);

        randomAccessFile.seek(startPoint.getValue());
        int contentLength = randomAccessFile.readInt();

        Pointer nextBlockPosition = readNextBlockPointer(randomAccessFile);

        byte[] bytes = new byte[contentLength];
        randomAccessFile.read(bytes);

        return new Block(nextBlockPosition, bytes);
    }

    /**
     * Returns pointer to next block of sequence.
     * The position in the {@code randomAccessFile} has to be already set to appropriate position.
     *
     * @param randomAccessFile {@link RandomAccessFile} to access a real file content
     * @return Pointer to next block or {@code null} if it is the last block of sequence
     * @throws IOException in case of IO errors
     */
    public static Pointer readNextBlockPointer(@NonNull RandomAccessFile randomAccessFile) throws IOException {
        long nextBlockPositionLong = randomAccessFile.readLong();
        Pointer nextBlockPosition = nextBlockPositionLong == 0 ? null : new Pointer(nextBlockPositionLong);
        if (nextBlockPosition != null) {
            assertPointerRange(nextBlockPosition, randomAccessFile);
        }
        return nextBlockPosition;
    }

}
