package com.jetbrains.virtualfs.blocks;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.RandomAccessFile;

@Slf4j
public class BlockWriter {

    /**
     * Writes block to file with offset from {@code startPoint}.
     *
     * @param block a block to write
     * @param startPoint offset in the real file
     * @param randomAccessFile {@link RandomAccessFile} for a real file
     * @throws IOException in case of IO errors
     */
    public static void writeBlock(Block block, Pointer startPoint, RandomAccessFile randomAccessFile) throws IOException {
        log.debug("write block at pointer {}", startPoint);

        randomAccessFile.seek(startPoint.getValue());
        randomAccessFile.writeInt(block.getLength());
        randomAccessFile.writeLong(block.getNextBlockPosition() == null ? 0 : block.getNextBlockPosition().getValue());
        randomAccessFile.write(block.getByteArray(), 0, block.getLength());
    }
}
