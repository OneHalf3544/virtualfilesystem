package com.jetbrains.virtualfs.blocks;

import lombok.EqualsAndHashCode;
import lombok.Value;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.OptionalLong;

/**
 * Block of file content.
 *
 * <p>The first four byte - size of current block (zero - no next block).
 * The next eight bytes - address of next segment
 * <pre>
 * +-----------------------------+
 * | The size of current block   |
 * +-----------------------------+
 * | A pointer to the next block |
 * +-----------------------------+
 * | ...                         |
 * | Block content               |
 * | ...                         |
 * |                             |
 * |                             |
 * +-----------------------------+
 * </pre>
 */
public class Block {

    public static final int BLOCK_SIZE = 64;
    public static final int METADATA_SIZE = Integer.BYTES + Long.BYTES;
    public static final int MAX_CONTENT_SIZE = BLOCK_SIZE - METADATA_SIZE;

    private final byte[] bytes;
    private final int length;
    private final Pointer nextBlockPosition;

    public Block(Pointer nextBlockPosition, byte[] bytes) {
        this(nextBlockPosition, bytes, bytes.length);
    }

    /**
     * Creates a block object instance.
     *
     * @param nextBlockPosition a pointer to next block in the sequence or {@code null} if this block is the last.
     * @param bytes content of block
     * @param length length of content (it can be less than {@code bytes} array size)
     */
    public Block(Pointer nextBlockPosition, byte[] bytes, int length) {
        assert bytes.length >= length : "length is more content of 'bytes' cannot fit into one block";
        assert length <= MAX_CONTENT_SIZE : "content of 'bytes' cannot fit into one block";

        this.nextBlockPosition = nextBlockPosition;
        this.bytes = bytes;
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    /**
     * Returns a byte of content by index.
     *
     * @param index index of the requested byte
     * @return requested byte
     */
    public byte getByte(int index) {
        if (index < 0 || index > getLength()) {
            throw new IndexOutOfBoundsException(index);
        }
        return bytes[index];
    }

    public Pointer getNextBlockPosition() {
        return nextBlockPosition;
    }

    @Override
    public String toString() {
        return String.format("Block with size %d, nextBlock at position %s", bytes.length, nextBlockPosition);
    }

    byte[] getByteArray() {
        return bytes;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Block block = (Block) other;
        return Arrays.equals(bytes, block.bytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }
}
