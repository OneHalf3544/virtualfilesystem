package com.jetbrains.virtualfs.blocks;

import lombok.NonNull;

import java.io.IOException;
import java.io.RandomAccessFile;

import static com.jetbrains.virtualfs.FileSystemImpl.assertPointerRange;
import static com.jetbrains.virtualfs.blocks.BlockReader.readNextBlockPointer;

public class BlockCounter {

    /**
     * Counts blocks in linked sequence that started from {@code startPoint}.
     *
     * @param startPoint beginning of the block sequence
     * @param randomAccessFile {@link RandomAccessFile} to get info from a real file
     * @return count of blocks in the sequence
     * @throws IOException in case of IO errors
     */
    public static long countBlocksInChain(@NonNull Pointer startPoint,
                                          @NonNull RandomAccessFile randomAccessFile) throws IOException {
        assertPointerRange(startPoint, randomAccessFile);

        Pointer pointer = startPoint;
        long result = 0;
        while (pointer != null) {
            randomAccessFile.seek(pointer.getValue() + Integer.BYTES);
            result++;
            pointer = readNextBlockPointer(randomAccessFile);
        }
        return result;
    }
}
