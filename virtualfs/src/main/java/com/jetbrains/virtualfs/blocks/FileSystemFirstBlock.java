package com.jetbrains.virtualfs.blocks;

import lombok.Value;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

import static com.jetbrains.virtualfs.blocks.Pointer.block;

@Value
public class FileSystemFirstBlock {

    private final Pointer rootDirectoryPointer;
    private final Pointer firstEmptyBlock;

    public static FileSystemFirstBlock initial() {
        return new FileSystemFirstBlock(block(1), block(2));
    }

    /**
     * Loads block from {@code file}.
     *
     * @param file {@link RandomAccessFile} to access the file content
     * @return loaded data
     * @throws IOException in case of IO errors
     */
    public static FileSystemFirstBlock fromFile(RandomAccessFile file) throws IOException {
        file.seek(0L);
        Pointer rootDirectoryPointer = new Pointer(file.readLong());
        Pointer firstEmptyBlock = new Pointer(file.readLong());
        return new FileSystemFirstBlock(rootDirectoryPointer, firstEmptyBlock);
    }

    /**
     * Converts content to {@link ByteBuffer}.
     *
     * @return result
     */
    public ByteBuffer toByteBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(Block.BLOCK_SIZE);
        buffer.putLong(rootDirectoryPointer.getValue());
        buffer.putLong(firstEmptyBlock.getValue());
        buffer.rewind();
        return buffer;
    }
}
