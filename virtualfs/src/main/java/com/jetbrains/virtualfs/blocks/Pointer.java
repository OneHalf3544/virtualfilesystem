package com.jetbrains.virtualfs.blocks;

import lombok.Value;

@Value
public class Pointer {

    private final long value;

    /**
     * Creates pointer to the block with given number.
     * number starts from zero.
     *
     * @param number index of blocks
     * @return pointer to the block
     */
    public static Pointer block(int number) {
        return new Pointer(number * Block.BLOCK_SIZE);
    }

    public Pointer nextBlock() {
        return new Pointer(value + Block.BLOCK_SIZE);
    }
}
