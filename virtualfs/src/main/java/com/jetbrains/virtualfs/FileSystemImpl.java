package com.jetbrains.virtualfs;

import com.jetbrains.virtualfs.blocks.Block;
import com.jetbrains.virtualfs.blocks.BlockReader;
import com.jetbrains.virtualfs.blocks.BlockWriter;
import com.jetbrains.virtualfs.blocks.FileSystemFirstBlock;
import com.jetbrains.virtualfs.blocks.Pointer;
import com.jetbrains.virtualfs.files.Directory;
import com.jetbrains.virtualfs.files.FsInputStream;
import com.jetbrains.virtualfs.files.FsOutputStream;
import com.jetbrains.virtualfs.files.VFile;
import com.jetbrains.virtualfs.preconditions.Preconditions;
import com.jetbrains.virtualfs.statistic.StatCollector;
import com.jetbrains.virtualfs.statistic.Statistic;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import javax.annotation.Nullable;

import static com.jetbrains.virtualfs.blocks.BlockReader.readNextBlockPointer;
import static com.jetbrains.virtualfs.preconditions.Preconditions.checkArgument;
import static com.jetbrains.virtualfs.preconditions.Preconditions.checkArgumentNotNull;
import static com.jetbrains.virtualfs.preconditions.Preconditions.checkFileIsNotRoot;
import static java.util.Objects.requireNonNull;

/**
 * Represents a file system. It works as wrapper over a real OS file.
 * First block of data contains those data:
 * <ul>
 *    <li>Link to the root directory</li>
 *    <li>Link to the first of unused block. All other unused blocks are chained in linked list manner</li>
 * </ul>
 *
 * <pre>
 *      +-------------------------------------------------------+
 *      | MetaInfo                                              |
 *      +-------------------------------------------------------+
 *      |       A link to the block with a root directory       +----+
 *      +-------------------------------------------------------+    |
 * +----+       A link to the first empty block                 |    |
 * |    +-------------------------------------------------------+    |
 * |    |                                                       |    |
 * |    |       Unused space                                    |    |
 * |    |                                                       |    |
 * |    +-------------------------------------------------------+    |
 * |    | The root directory content                            <----+
 * |    +-------------------------------------------------------+
 * |    |       file name and pointer to the first file block   |
 * |    +-------------------------------------------------------+
 * |    |       file name and pointer to the first file block   |
 * |    +-------------------------------------------------------+
 * |    |       ...                                             |
 * |    +-------------------------------------------------------+
 * +----> Empty space block.                                    |
 *      +-------------------------------------------------------+
 *      |       Link to the next empty block (or '0')           +----+
 *      +-------------------------------------------------------+    |
 *      |       ...                                             |    |
 *      |                                                       |    |
 *      +-------------------------------------------------------+    |
 *      |                                                       <----+
 *      | Empty space block                                     |
 *      +-------------------------------------------------------+
 *      |       ...                                             |
 *      +-------------------------------------------------------+
 * </pre>
 */
@Slf4j
public class FileSystemImpl implements FileSystem {

    private static final Pointer ROOT_DIRECTORY_POINTER = Pointer.block(1);

    private final File fsFile;
    private final StatCollector statCollector = new StatCollector();
    private RandomAccessFile randomAccessFile;

    private Pointer firstUnusedBlock;

    FileSystemImpl(@NonNull File fsFile,
                   @NonNull RandomAccessFile randomAccessFile,
                   @NonNull Pointer firstUnusedBlock) {
        this.fsFile = fsFile;
        this.randomAccessFile = randomAccessFile;
        this.firstUnusedBlock = firstUnusedBlock;

    }

    @Override
    public void importFile(File outerFile, VFile innerDirectory) throws IOException {
        if (!outerFile.exists()) {
            throw new FileNotFoundException("cannot found file " + outerFile);
        }

        if (outerFile.isDirectory()) {
            // copy the directory content.
            VFile directory = createDirectory(innerDirectory, sanitizeName(outerFile));
            for (File file : requireNonNull(outerFile.listFiles())) {
                importFile(file, directory);
            }
        } else {
            // copy the file
            try (InputStream input = new FileInputStream(outerFile)) {
                writeFile(new VFile(innerDirectory.getName() + sanitizeName(outerFile)), input);
            }
        }
    }

    private String sanitizeName(File outerFile) {
        return outerFile.getName().replaceAll("[^\\w\\d._]", "_");
    }

    @Override
    public void exportFile(VFile innerFile, File outerFile) throws IOException {
        if (!exists(innerFile)) {
            throw new FileNotFoundException("cannot found file " + innerFile);
        }

        if (innerFile.isDirectory()) {
            // copy the directory content.
            if (outerFile.exists() && !outerFile.isDirectory()) {
                throw new IOException("file '" + outerFile + "' already exists and it's not a directory");
            } else {
                FileUtils.forceMkdir(outerFile);
            }
            Directory directory = loadDirectory(innerFile);
            for (String name : requireNonNull(directory.getNames())) {
                exportFile(new VFile(innerFile.getName() + name), new File(outerFile, name));
            }
        } else {
            // copy the file
            try (OutputStream output = new FileOutputStream(outerFile)) {
                readFromFile(innerFile).transferTo(output);
            }
        }
    }

    @Override
    public Directory loadDirectory(VFile directory) throws IOException {
        return getDirectory(directory);
    }

    @Override
    public VFile writeFile(VFile file, InputStream inputStream) throws IOException {
        log.info("create file {}", file);

        // write file content
        Pointer filePointer = reserveBlock();
        writeToBlockSequence(filePointer, inputStream);

        // save new data of a directory
        Directory parentDirectory = getDirectory(file.getParent());
        Pointer replacedFilePointer = parentDirectory.replaceFilePointer(file.getShortName(), filePointer);
        storeDirectoryContent(parentDirectory);

        addBlockSequenceToEmptyBlocks(replacedFilePointer);
        return file;
    }

    @Override
    public VFile createDirectory(VFile parent, @NonNull String directoryName) throws IOException {
        log.info("create the directory {} in file", directoryName, parent);

        Directory parentDirectory = loadDirectory(parent);
        if (parentDirectory.getNames().contains(directoryName + "/")) {
            throw new IllegalArgumentException(parent + " already contains directory " + directoryName);
        }

        // write file content
        Pointer filePointer = reserveBlock();
        Directory newDirectory = Directory.emptyDirectory(filePointer);
        writeToBlockSequence(filePointer, newDirectory.toInputStream());

        // save new data of a directory
        parentDirectory.addPointerAsFile(
                directoryName + "/", filePointer,
                () -> addBlockSequenceToEmptyBlocks(filePointer));
        storeDirectoryContent(parentDirectory);

        return new VFile(parent, directoryName + "/");
    }

    @Override
    public boolean exists(VFile file) throws IOException {
        return file.isRoot() || getDirectory(file.getParent()).getNames().contains(file.getShortName());
    }

    @Override
    public void remove(VFile file) throws IOException {
        log.info("delete {}", file);
        checkFileIsNotRoot(file, "cannot remove root directory");

        if (file.isDirectory()) {
            Directory directory = loadDirectory(file);
            for (String name : directory.getNames()) {
                remove(new VFile(file.getName() + name));
            }
        }

        Directory parent = getDirectory(file.getParent());
        Pointer pointerToDeletedFile = parent.deleteFile(file.getShortName());
        checkArgumentNotNull(pointerToDeletedFile, () -> "no such file or directory: " + file);

        storeDirectoryContent(parent);

        addBlockSequenceToEmptyBlocks(pointerToDeletedFile);
    }

    private void addBlockSequenceToEmptyBlocks(@Nullable Pointer pointerToDeletedFile) throws IOException {
        if (pointerToDeletedFile == null) {
            return;
        }
        synchronized (this) {
            FileSystemFirstBlock firstBlock = FileSystemFirstBlock.fromFile(randomAccessFile);
            Pointer firstEmptyBlock = firstBlock.getFirstEmptyBlock();

            extendSequenceByPointer(pointerToDeletedFile, firstEmptyBlock);

            randomAccessFile.seek(0);
            FileSystemFirstBlock newFirstBlockContent = new FileSystemFirstBlock(
                    firstBlock.getRootDirectoryPointer(),
                    pointerToDeletedFile);
            this.firstUnusedBlock = pointerToDeletedFile;
            randomAccessFile.getChannel().write(newFirstBlockContent.toByteBuffer());
        }
    }

    private void extendSequenceByPointer(@NonNull Pointer pointerToDeletedFile,
                                         @NonNull Pointer firstEmptyBlock) throws IOException {
        Pointer pointer = pointerToDeletedFile;
        while (true) {
            randomAccessFile.seek(pointer.getValue() + Integer.BYTES);
            Pointer nextPointer = readNextBlockPointer(randomAccessFile);
            if (nextPointer == null) {
                randomAccessFile.seek(pointer.getValue() + Integer.BYTES);
                randomAccessFile.writeLong(firstEmptyBlock.getValue());
                return;
            }
            pointer = nextPointer;
        }
    }

    @Override
    public void rename(VFile oldName, VFile newName) throws IOException {
        log.info("rename {} to {}", oldName, newName);
        checkFileIsNotRoot(oldName, "old name cannot be a root directory");
        checkFileIsNotRoot(newName, "new name cannot be a root directory");

        checkArgument(
                oldName.isDirectory() == newName.isDirectory(),
                () -> String.format("cannot change type from '%s' to '%s'", toType(oldName), toType(newName)));
        checkArgument(
                !newName.getName().startsWith(oldName.getName()),
                "cannot move a file or a directory into itself");

        Directory parent = getDirectory(oldName.getParent());
        Pointer pointer = parent.deleteFile(oldName.getShortName());
        checkArgumentNotNull(pointer, () -> "no such file or directory: " + oldName);

        if (oldName.getParent().equals(newName.getParent())) {
            justRename(parent, pointer, newName);
        } else {
            moveToAnotherDirectory(parent, newName, pointer);
        }

    }

    private String toType(VFile newName) {
        return newName.isDirectory() ? "directory" : "file";
    }

    @Override
    @SneakyThrows
    public Statistic getStatistics() {
        return statCollector.analyse(this);
    }

    @Override
    public File getSystemFile() {
        return fsFile;
    }

    private void moveToAnotherDirectory(Directory oldParent, VFile newName, Pointer pointer) throws IOException {
        Directory newParent = getDirectory(newName.getParent());
        newParent.addPointerAsFile(newName.getShortName(), pointer, () -> { });
        storeDirectoryContent(oldParent);
        storeDirectoryContent(newParent);
    }

    private void justRename(Directory parent, Pointer pointer, VFile newName) throws IOException {

        parent.addPointerAsFile(newName.getShortName(), pointer, () -> { });
        storeDirectoryContent(parent);
    }

    private void storeDirectoryContent(Directory directory) throws IOException {
        randomAccessFile.seek(directory.getPointer().getValue() + Integer.BYTES);
        long nextBlock = randomAccessFile.readLong();
        writeToBlockSequence(directory.getPointer(), directory.toInputStream());
        if (nextBlock != 0) {
            addBlockSequenceToEmptyBlocks(new Pointer(nextBlock));
        }
    }

    /**
     * Request a block that can be used to write a new content.
     * It can be a block that stayed unused after a previously deleted file or can be created at the end of the system file.
     *
     * @return a pointer to reserved block
     * @throws IOException in case of IO errors
     */
    public synchronized Pointer reserveBlock() throws IOException {
        log.debug("reserve empty block");

        Pointer result = firstUnusedBlock;
        Block block = BlockReader.readBlock(firstUnusedBlock, randomAccessFile);
        Pointer newBlockPoint = block.getNextBlockPosition();
        if (newBlockPoint == null) {
            firstUnusedBlock = result.nextBlock();
            BlockWriter.writeBlock(new Block(null, new byte[0]), firstUnusedBlock, randomAccessFile);
        } else {
            firstUnusedBlock = newBlockPoint;
        }
        randomAccessFile.seek(Long.BYTES);
        randomAccessFile.writeLong(firstUnusedBlock.getValue());
        return result;
    }

    private Directory getDirectory(@NonNull VFile directoryName) throws IOException {
        checkArgument(directoryName.isDirectory(), () -> "directory was expected, but got " + directoryName);

        if (directoryName.isRoot()) {
            try (InputStream streamFromPointer = readFromPointer(ROOT_DIRECTORY_POINTER)) {
                return Directory.fromInputStream(ROOT_DIRECTORY_POINTER, streamFromPointer);
            }
        }
        Directory parentOfParent = getDirectory(directoryName.getParent());
        Pointer pointer = parentOfParent.getPointerByName(directoryName.getShortName());
        try (InputStream streamFromPointer = readFromPointer(pointer)) {
            return Directory.fromInputStream(pointer, streamFromPointer);
        }
    }

    @Override
    public String toString() {
        return fsFile.toString();
    }

    public static void assertPointerRange(@NonNull Pointer pointer,
                                          @NonNull RandomAccessFile randomAccessFile) throws IOException {
        assert pointer.getValue() < randomAccessFile.length()
                : "pointer value (" + pointer + ") is more than a file length (" + randomAccessFile.length() + ")";
    }

    private void writeToBlockSequence(Pointer pointer, InputStream inputStream) throws IOException {
        try (OutputStream outputStream = new FsOutputStream(pointer, this)) {
            inputStream.transferTo(outputStream);
        }
    }

    @Override
    public InputStream readFromFile(VFile file) throws IOException {
        log.info("read file {}", file);
        checkArgument(!file.isDirectory(), () -> "cannot read a directory as a file");

        Pointer pointerByName = getDirectory(file.getParent()).getPointerByName(file.getShortName());
        assertPointerRange(pointerByName, randomAccessFile);
        return new FsInputStream(pointerByName, fsFile);
    }

    private InputStream readFromPointer(Pointer pointer) throws IOException {
        return new FsInputStream(pointer, fsFile);
    }

    @Override
    public void close() throws IOException {
        randomAccessFile.close();
    }

}
