package com.jetbrains.virtualfs.statistic;

import com.jetbrains.virtualfs.FileSystem;
import com.jetbrains.virtualfs.blocks.Block;
import com.jetbrains.virtualfs.blocks.BlockCounter;
import com.jetbrains.virtualfs.blocks.FileSystemFirstBlock;
import com.jetbrains.virtualfs.files.Directory;
import com.jetbrains.virtualfs.files.VFile;
import lombok.SneakyThrows;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

/**
 * The util to get file system status.
 * Used to find any problems like cyclic links or lost blocks.
 */
public class StatCollector {

    /**
     * Collects a statistic for a file system.
     *
     * @param fileSystem checked file system
     * @return collected stats
     * @throws IOException in case of reading content problem
     */
    public Statistic analyse(FileSystem fileSystem) throws IOException {
        try (RandomAccessFile raFile = new RandomAccessFile(fileSystem.getSystemFile(), "r")) {

            Statistic statistic = analyse(fileSystem, "/", raFile);

            FileSystemFirstBlock firstBlock = FileSystemFirstBlock.fromFile(raFile);
            long rootDirSize = BlockCounter.countBlocksInChain(firstBlock.getRootDirectoryPointer(), raFile);
            long emptyBlocksCount = BlockCounter.countBlocksInChain(firstBlock.getFirstEmptyBlock(), raFile);

            long usedBlocksCount = 1 + statistic.getUsedBlocksCount() + rootDirSize;
            long totalBlocksCount = (fileSystem.getSystemFile().length() + Block.BLOCK_SIZE - 1) / Block.BLOCK_SIZE;

            return statistic.toBuilder()
                    .directoriesCount(statistic.getDirectoriesCount() + 1)
                    .totalBlocksCount(totalBlocksCount)
                    .emptyBlocksCount(emptyBlocksCount)
                    .lostSpaceCount(totalBlocksCount - usedBlocksCount - emptyBlocksCount)
                    .usedBlocksCount(usedBlocksCount)
                    .build();
        }
    }

    @SneakyThrows
    private Statistic analyse(FileSystem fileSystem, String directoryName, RandomAccessFile raFile) {
        Directory directory = fileSystem.loadDirectory(new VFile(directoryName));

        List<String> directoriesNames = directory.getNames().stream()
                .filter(name -> name.endsWith("/"))
                .collect(toList());

        Optional<Statistic> subDirectoriesStat = directoriesNames.stream()
                .map(subdir -> analyse(fileSystem, directoryName + subdir, raFile))
                .reduce(Statistic::plus);

        Statistic currentDirectoryStat = Statistic.builder()
                .directoriesCount(directoriesNames.size())
                .filesCount(directory.getNames().size() - directoriesNames.size())
                .maxDepth(subDirectoriesStat.map(Statistic::getMaxDepth).map(i -> i + 1).orElse(0L))
                .usedBlocksCount(blocksUsedByDirectory(directory, raFile))
                .build();

        if (!subDirectoriesStat.isPresent()) {
            return currentDirectoryStat;
        }

        return currentDirectoryStat.plus(subDirectoriesStat.get());
    }

    @SneakyThrows
    private long blocksUsedByDirectory(Directory directory, RandomAccessFile raFile) {
        long blocksUsedInCurrentDirectory = 0;
        for (String name : directory.getNames()) {
            blocksUsedInCurrentDirectory += BlockCounter.countBlocksInChain(directory.getPointerByName(name), raFile);
        }
        return blocksUsedInCurrentDirectory;
    }


}
