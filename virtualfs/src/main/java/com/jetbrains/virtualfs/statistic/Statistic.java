package com.jetbrains.virtualfs.statistic;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class Statistic {

    long directoriesCount;
    long filesCount;
    long maxDepth;

    long emptyBlocksCount;
    long lostSpaceCount;
    long usedBlocksCount;
    long totalBlocksCount;

    Statistic plus(Statistic statistic) {
        return Statistic.builder()
                .directoriesCount(this.directoriesCount + statistic.directoriesCount)
                .filesCount(this.filesCount + statistic.filesCount)
                .maxDepth(Math.max(this.maxDepth, statistic.maxDepth))
                .usedBlocksCount(this.usedBlocksCount + statistic.usedBlocksCount)
                .emptyBlocksCount(this.emptyBlocksCount + statistic.emptyBlocksCount)
                .lostSpaceCount(this.lostSpaceCount + statistic.lostSpaceCount)
                .totalBlocksCount(this.totalBlocksCount + statistic.totalBlocksCount)
                .build();
    }
}
