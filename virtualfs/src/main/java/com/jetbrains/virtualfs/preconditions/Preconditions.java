package com.jetbrains.virtualfs.preconditions;

import com.jetbrains.virtualfs.files.VFile;

import java.util.function.Supplier;

import static java.util.Objects.nonNull;


/**
 * A helper class for assertions.
 * In contrast of {@code assert} keyword, works at runtime
 * and can be used for check external arguments (like user input, for example)
 */
public class Preconditions {

    public static void checkArgumentNotNull(Object object, Supplier<String> errorMessageSupplier) {
        checkArgument(nonNull(object), errorMessageSupplier);
    }

    public static void checkFileIsNotRoot(VFile file, String errorMessage) throws IllegalArgumentException {
        checkArgument(!file.isRoot(), () -> errorMessage);
    }

    public static void checkArgument(boolean condition, String errorMessage) throws IllegalArgumentException {
        checkArgument(condition, () -> errorMessage);
    }

    /**
     * Throws exception if the condition is not true.
     *
     * @param condition result of some external boolean check
     * @param errorMessageSupplier supplier of lazy evaluated string with an error message
     * @throws IllegalArgumentException if condition is false
     */
    public static void checkArgument(boolean condition,
                                     Supplier<String> errorMessageSupplier) throws IllegalArgumentException {
        if (!condition) {
            throw new IllegalArgumentException(errorMessageSupplier.get());
        }
    }
}
