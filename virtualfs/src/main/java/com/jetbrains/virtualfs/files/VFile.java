package com.jetbrains.virtualfs.files;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.regex.Pattern;

import static com.jetbrains.virtualfs.preconditions.Preconditions.checkArgument;

/**
 * Represents a file name.
 * The name can contains only latin letters, digits, chars {@code '_'}, {@code '.'} and {@code '-'}
 *
 * <p>Directories in a path separated by {@code /}.
 * The slash char is a part of directory name, so if the last char is a slash, it means that name belongs to a directory.
 * An absolute name cannot contain two slashes in a row and has to starts from a leading slash.
 *
 * <p>Date: 26.05.2018
 *
 * @author OneHalf
 */
@Getter
@EqualsAndHashCode(of = "name")
public class VFile {

    private static final Pattern SHORT_NAME_REGEX = Pattern.compile("[\\w\\d_.-]+");

    public VFile(VFile parent, String fileName) {
        this(parent.getName() + fileName);
    }

    public static boolean isCorrectShortName(String name) {
        return SHORT_NAME_REGEX.matcher(name).matches();
    }

    private final String name;
    private final String shortName;
    private final VFile parent;
    private final boolean directory;


    /**
     * Creates an instance.
     *
     * @param name an absolute name to a file or a directory.
     */
    public VFile(String name) {
        checkAbsoluteName(name);

        this.directory = name.endsWith("/");
        this.name = name;

        int splitIndex = name.lastIndexOf("/", name.length() - 2) + 1;
        if (splitIndex == 0) {
            parent = null;
            shortName = name;
        } else {
            this.shortName = name.substring(splitIndex);
            this.parent = new VFile(name.substring(0, splitIndex));
        }

    }

    private void checkAbsoluteName(String name) {
        checkArgument(name.startsWith("/"), "full name was expected");
        checkArgument(!name.contains("//"), "full name cannot contain two slashed in a row");
        checkArgument(
                isCorrectShortName(name.replaceAll("/", "_")),
                () -> "name should have format '" + SHORT_NAME_REGEX + "', but was: " + name);
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isRoot() {
        return parent == null;
    }
}
