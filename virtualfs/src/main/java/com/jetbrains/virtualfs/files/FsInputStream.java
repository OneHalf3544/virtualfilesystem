package com.jetbrains.virtualfs.files;

import com.jetbrains.virtualfs.blocks.Block;
import com.jetbrains.virtualfs.blocks.BlockReader;
import com.jetbrains.virtualfs.blocks.Pointer;
import lombok.ToString;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

/**
 * An input stream of file system.
 * Encapsulates the logic of reading linked blocks.
 */
@ToString(of = "startPointer")
public class FsInputStream extends InputStream {

    private final RandomAccessFile randomAccessFile;
    private final Pointer startPointer;

    private int currentPositionInBlock = 0;
    private Block currentBlock;

    /**
     * Creates an input stream to read data from the {@code startPoint} position.
     *
     * @param startPoint a pointer to the first block
     * @param fsFile a real system file
     * @throws IOException in case of IO errors
     */
    public FsInputStream(Pointer startPoint, File fsFile) throws IOException {
        this.startPointer = startPoint;
        this.randomAccessFile = new RandomAccessFile(fsFile, "r");
        currentBlock = BlockReader.readBlock(startPoint, randomAccessFile);
    }

    @Override
    public int read() throws IOException {
        if (currentPositionInBlock < currentBlock.getLength()) {
            return currentBlock.getByte(currentPositionInBlock++);
        }
        if (currentBlock.getNextBlockPosition() == null) {
            return -1;
        }
        currentBlock = BlockReader.readBlock(currentBlock.getNextBlockPosition(), randomAccessFile);
        currentPositionInBlock = 0;
        return read();
    }

    @Override
    public void close() throws IOException {
        randomAccessFile.close();
    }
}
