package com.jetbrains.virtualfs.files;

import com.jetbrains.virtualfs.blocks.Pointer;
import lombok.NonNull;
import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class Directory {

    public static final int MAX_FILENAME_LENGTH = 200;

    public static Directory emptyDirectory(Pointer pointer) {
        return new Directory(pointer, new HashMap<>());
    }

    /**
     * Deserialize directory content from the {@code inputStream}.
     *
     * @param pointer a link to the first block of the sequence that contains this directory (it's pointer to self)
     * @param inputStream a stream for deserializing
     * @return deserialization result
     * @throws IOException in case of IO errors
     */
    public static Directory fromInputStream(@NonNull Pointer pointer,
                                            @NonNull InputStream inputStream) throws IOException {
        ByteBuffer buffer = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));

        Map<String, Pointer> map = new HashMap<>();
        while (buffer.hasRemaining()) {
            String fileName = readFileName(buffer);
            map.put(fileName, new Pointer(buffer.getLong()));
        }
        return new Directory(pointer, map);
    }

    private static String readFileName(ByteBuffer buffer) {
        int fileNameSize = buffer.getInt();
        if (fileNameSize > MAX_FILENAME_LENGTH) {
            throw new IllegalArgumentException("unexpected size of a file name: " + fileNameSize);
        }
        byte[] nameBytes = new byte[fileNameSize];
        buffer.get(nameBytes);
        return new String(nameBytes, StandardCharsets.UTF_8);
    }

    private final Pointer pointer;
    private final Map<String, Pointer> files;

    public Directory(Pointer pointer, Map<String, Pointer> files) {
        this.pointer = pointer;
        this.files = files;
    }

    /**
     * Serialize the directory to an {@code InputStream}.
     *
     * @return serialized content
     */
    public InputStream toInputStream() {
        Iterator<ByteBuffer> iterator = files.entrySet().stream()
                .map(this::entryToByteArray)
                .iterator();

        return new InputStream() {
            ByteBuffer array = iterator.hasNext() ? iterator.next() : null;

            @Override
            public int read() {
                if (array == null) {
                    return -1;
                }
                if (array.hasRemaining()) {
                    return array.get();
                }
                array = iterator.hasNext() ? iterator.next() : null;
                return read();
            }
        };
    }

    private ByteBuffer entryToByteArray(Map.Entry<String, Pointer> entry) {
        byte[] nameBytes = entry.getKey().getBytes(StandardCharsets.UTF_8);
        ByteBuffer bytes = ByteBuffer.allocate(Integer.BYTES + nameBytes.length + Long.BYTES);
        bytes.putInt(nameBytes.length);
        bytes.put(nameBytes);
        bytes.putLong(entry.getValue().getValue());
        bytes.flip();
        return bytes;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Directory directory = (Directory) other;
        return Objects.equals(files, directory.files);
    }

    @Override
    public int hashCode() {

        return Objects.hash(files);
    }

    /**
     * Returns a pointer to the first block of the file content.
     *
     * @param name file name
     * @return pointer to the file content
     * @throws FileNotFoundException if directory doesn't contain requested file
     */
    public Pointer getPointerByName(String name) throws FileNotFoundException {
        Pointer pointer = files.get(name);
        if (pointer == null) {
            throw new FileNotFoundException("name: " + name);
        }
        return pointer;
    }

    public Set<String> getNames() {
        return files.keySet();
    }

    public boolean isEmpty() {
        return files.isEmpty();
    }

    @Override
    public String toString() {
        return "Directory{files=" + files + '}';
    }

    /**
     * Add a file pointer to directory.
     *
     * @param simpleName short filename
     * @param pointer pointer to file content
     * @param rollback rollback logic
     * @throws IOException in case of io error
     */
    public void addPointerAsFile(String simpleName, Pointer pointer, Rollback rollback) throws IOException {
        if (files.containsKey(simpleName)) {
            rollback.run();
            throw new IllegalArgumentException("file " + simpleName + " already exists in the directory");
        }
        files.put(simpleName, pointer);
    }

    public Pointer replaceFilePointer(String simpleName, Pointer pointer) {
        return files.put(simpleName, pointer);
    }

    public Pointer getPointer() {
        return pointer;
    }

    public Pointer deleteFile(String name) {
        return files.remove(name);
    }

    public interface Rollback {

        void run() throws IOException;
    }
}
