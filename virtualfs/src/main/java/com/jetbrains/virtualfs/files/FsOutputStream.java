package com.jetbrains.virtualfs.files;

import com.jetbrains.virtualfs.FileSystemImpl;
import com.jetbrains.virtualfs.blocks.Block;
import com.jetbrains.virtualfs.blocks.BlockWriter;
import com.jetbrains.virtualfs.blocks.Pointer;
import lombok.ToString;

import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

@ToString(of = "startPointer")
public class FsOutputStream extends OutputStream {

    private final RandomAccessFile randomAccessFile;
    private final FileSystemImpl fileSystem;
    private final Pointer startPointer;

    private int currentPositionInBuffer = 0;
    private Pointer currentBlockStartPoint;
    private byte[] buffer = new byte[Block.MAX_CONTENT_SIZE];

    /**
     * Creates a output stream from a block sequence that started from {@code pointer}.
     *
     * @param pointer a pointer to the first block of a sequence.
     *                A new blocks will be reserved when needed.
     * @param fileSystem the virtual filesystem where we are going to write content
     * @throws IOException in case of IO errors
     */
    public FsOutputStream(Pointer pointer, FileSystemImpl fileSystem) throws IOException {
        this.startPointer = pointer;
        this.randomAccessFile = new RandomAccessFile(fileSystem.getSystemFile(), "rw");
        this.fileSystem = fileSystem;
        this.currentBlockStartPoint = pointer;
    }

    @Override
    public void write(int b) throws IOException {
        if (currentPositionInBuffer < Block.MAX_CONTENT_SIZE) {
            buffer[currentPositionInBuffer++] = (byte) b;
            return;
        }
        addNewBlock();
        write(b);
    }

    private void addNewBlock() throws IOException {
        Pointer nextBlock = fileSystem.reserveBlock();
        flush(nextBlock);
        currentBlockStartPoint = nextBlock;
        currentPositionInBuffer = 0;
    }

    private void flush(Pointer nextBlock) throws IOException {
        BlockWriter.writeBlock(new Block(nextBlock, buffer, currentPositionInBuffer), currentBlockStartPoint, randomAccessFile);
    }

    @Override
    public void close() throws IOException {
        flush(null);
        randomAccessFile.close();
    }
}
