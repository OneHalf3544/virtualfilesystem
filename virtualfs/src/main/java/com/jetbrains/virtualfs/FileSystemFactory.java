package com.jetbrains.virtualfs;

import com.jetbrains.virtualfs.blocks.Block;
import com.jetbrains.virtualfs.blocks.FileSystemFirstBlock;
import com.jetbrains.virtualfs.blocks.Pointer;
import com.jetbrains.virtualfs.files.Directory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

import static com.jetbrains.virtualfs.blocks.Pointer.block;

/**
 * Loads or creates a virtual file system.
 */
@Slf4j
public class FileSystemFactory {

    private static final byte[] EMPTY_FILE_SYSTEM_CONTENT = emptyFileSystemContent();

    /**
     * Creates a virtual filesystem in the {@code fsFile}.
     *
     * @param fsFile a real file
     * @return an object that represents created virtual fs.
     * @throws IOException in case of IO errors
     */
    public static FileSystem createFileSystem(File fsFile) throws IOException {
        log.info("create file system on file {}", fsFile);
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(fsFile, "rw")) {
            randomAccessFile.write(EMPTY_FILE_SYSTEM_CONTENT);
        }
        return loadFileSystem(fsFile);
    }

    /**
     * Loads virtual file system from a real file.
     *
     * @param fsFile file that contains virtualfs data
     * @return object that represented loaded system
     * @throws IOException in case of IO errors
     */
    public static FileSystem loadFileSystem(File fsFile) throws IOException {
        log.info("load file system from file {}", fsFile);
        RandomAccessFile randomAccessFile = new RandomAccessFile(fsFile, "rw");
        randomAccessFile.seek(Long.BYTES);
        Pointer firstUnusedBlock = new Pointer(randomAccessFile.readLong());
        return new FileSystemImpl(fsFile, randomAccessFile, firstUnusedBlock);
    }

    /**
     * Prepare file system content.
     *
     * @return an etalon byte buffer with empty file system content.
     */
    static byte[] emptyFileSystemContent() {
        // Initial content has three blocks:
        // 1. metadata block,
        // 2. empty rootDirectory
        // 3. reserved block with empty space

        byte[] fileSystemInitialImage = new byte[3 * Block.BLOCK_SIZE];
        ByteBuffer firstBlock = FileSystemFirstBlock.initial().toByteBuffer();
        firstBlock.get(fileSystemInitialImage, 0, firstBlock.remaining());
        copyBlockStreamToArray(Directory.emptyDirectory(block(1)).toInputStream(), fileSystemInitialImage, block(1));

        return fileSystemInitialImage;
    }

    @SneakyThrows
    private static void copyBlockStreamToArray(InputStream sourceStream, byte[] receiver, Pointer pointer) {
        byte[] writenContent = IOUtils.toByteArray(sourceStream);
        assert writenContent.length <= Block.MAX_CONTENT_SIZE : "too long sourceStream (" + writenContent.length + ")";

        ByteBuffer buffer = ByteBuffer.wrap(receiver);
        buffer.position((int) pointer.getValue());
        buffer.putInt(writenContent.length);
        buffer.putLong(0L);
        buffer.put(writenContent);
    }
}
