package com.jetbrains.virtualfs.blocks

import nl.jqno.equalsverifier.EqualsVerifier
import spock.lang.Specification

import static com.jetbrains.virtualfs.blocks.Pointer.block

class BlockTest extends Specification {

    def "should return bytes array length as length of the block"() {
        given: "a block with ten bytes"
        def block = new Block(block(0), new byte[10])

        expect: "getLength() returns 10"
        block.length == 10
    }

    def 'equals method should be implemented correctly'() {
        expect: "correct equals behaviour"
        EqualsVerifier.forClass(Block)
                .usingGetClass()
                .withOnlyTheseFields("bytes")
                .verify();
    }
}
