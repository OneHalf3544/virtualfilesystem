package com.jetbrains.virtualfs.blocks

import spock.lang.Specification

import java.nio.charset.StandardCharsets

import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.eq
import static org.mockito.Mockito.*

class BlockWriterTest extends Specification {

    def "should write stream and record correct length at start of block"() {
        given: "a block with size less than allowed by block"
        def buffer = new byte[Block.MAX_CONTENT_SIZE]

        and: "content takes only part of byte buffer"
        def content = "content".getBytes(StandardCharsets.UTF_8)
        content.eachWithIndex { byte b, int i -> buffer[i] = b}
        def block = new Block(null, buffer, content.length)

        def randomAccessFile = mock(RandomAccessFile)

        when: "write the block to file"
        BlockWriter.writeBlock(block, Pointer.block(1), randomAccessFile)

        then: "randomAccessFile called with correct length parameter"
        verify(randomAccessFile).seek(Pointer.block(1).getValue())
        verify(randomAccessFile, atLeastOnce()).writeInt(content.length)
        verify(randomAccessFile, atLeastOnce()).writeLong(0L)
        verify(randomAccessFile).write(any(byte[].class), eq(0), eq(content.length))

    }
}
