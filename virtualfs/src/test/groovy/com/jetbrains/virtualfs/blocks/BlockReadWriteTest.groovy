package com.jetbrains.virtualfs.blocks

import com.jetbrains.virtualfs.FileSystemTest
import spock.lang.Specification

class BlockReadWriteTest extends Specification {

    def "we should read the same data we stored into file when data fits to one block"() {
        given: "a temp file and a block data"
        def file = FileSystemTest.createTempFile()
        def blockPointer = Pointer.block(0)
        def block = new Block(null, 'byte content'.getBytes())
        def randomAccessFile = new RandomAccessFile(file, "rw")

        when: "write block to file"
        BlockWriter.writeBlock(block, blockPointer, randomAccessFile)

        then: "file contains data"
        file.length() > block.length

        and: "we can read that content back"
        BlockReader.readBlock(blockPointer, randomAccessFile) == block
    }
}
