package com.jetbrains.virtualfs

import com.jetbrains.virtualfs.files.VFile
import spock.lang.Specification

/**
 *
 *
 * Date: 27.05.2018
 * @author OneHalf
 */
class FileSystemFactoryTest extends Specification {

    def "content for empty fs should be not empty"() {
        given: "a buffer filled with an initial fs content"
        def emptyFS = FileSystemFactory.emptyFileSystemContent()

        expect:
        emptyFS.any {it != 0 }
    }

    def "we can load fs that we just have created"() {
        given: "a file for virtual fs"
        def file = File.createTempFile("virturalfs-", ".vfs")

        when: "we created a file system"
        def createdFs = FileSystemFactory.createFileSystem(file)
        createdFs.close()
        and: "load it again"
        createdFs = FileSystemFactory.loadFileSystem(file)

        then: "no errors"
        noExceptionThrown()
        createdFs.loadDirectory(new VFile("/")).isEmpty()
    }
}
