package com.jetbrains.virtualfs

import com.jetbrains.virtualfs.blocks.Block
import com.jetbrains.virtualfs.files.VFile
import com.jetbrains.virtualfs.statistic.Statistic
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll

import static org.hamcrest.core.IsCollectionContaining.hasItem
import static spock.util.matcher.HamcrestSupport.expect

@Unroll
@Stepwise
class FileSystemTest extends Specification {

    @Shared
    private FileSystemImpl fileSystem

    def "new filesystem contains no files"() {
        given: 'new file system'
        fileSystem = createFileSystem()

        when: 'get list of root directory'
        def files = this.fileSystem.loadDirectory(new VFile('/'))


        then: 'fs contains no files'
        verifyAll {
            files.isEmpty()
            fileSystem.getStatistics().maxDepth == 0
            fileSystem.getStatistics().filesCount == 0
            fileSystem.getStatistics().directoriesCount == 1
            fileSystem.getStatistics().usedBlocksCount == 2
            fileSystem.getStatistics().totalBlocksCount == 3
            fileSystem.getStatistics().lostSpaceCount == 0
            fileSystem.getStatistics().emptyBlocksCount == 1

            this.fileSystem.fsFile.size() > 2 * Block.BLOCK_SIZE
            this.fileSystem.fsFile.size() <= 3 * Block.BLOCK_SIZE
        }
    }


    def "a new file persists in a filesystem"() {
        when: 'save new file'
        def file = fileSystem.writeFile(f('/a_new_file.txt'), IOUtils.toInputStream('file content'))

        then: 'it allows to read stored content'
        expect fileSystem.loadDirectory(f('/')).getNames(), hasItem(file.shortName)
        IOUtils.toString(fileSystem.readFromFile(f("/a_new_file.txt"))) == 'file content'
    }


    def "we can change a file content"() {
        when: 'save new content into existing file'
        def file = fileSystem.writeFile(f('/a_new_file.txt'), IOUtils.toInputStream('another file content'))

        then: 'it reads the new content'
        expect fileSystem.loadDirectory(f('/')).getNames(), hasItem(file.shortName)
        IOUtils.toString(fileSystem.readFromFile(f("/a_new_file.txt"))) == 'another file content'
    }


    def "we can create a sub directory"() {
        when: 'create a subdirectory'
        def subdirectory = fileSystem.createDirectory(f('/'), 'subdirectory')

        then: "it doesn't have content"
        subdirectory.isDirectory()
        fileSystem.loadDirectory(f('/subdirectory/')).isEmpty()
    }


    def "we can read/write file from a subdirectory"() {
        when: 'create a file'
        def file = fileSystem.writeFile(f('/subdirectory/file.txt'), IOUtils.toInputStream('file from a subdirectory'))

        then: "we can read it back"
        !file.isDirectory()
        file.getName() == "/subdirectory/file.txt"
        expect fileSystem.loadDirectory(f('/subdirectory/')).getNames(), hasItem("file.txt")
        IOUtils.toString(fileSystem.readFromFile(f('/subdirectory/file.txt'))) == "file from a subdirectory"
    }

    def "we can rename a directory"() {
        when: "rename dir"
        fileSystem.rename(f("/subdirectory/"), f("/dir/"))

        then: "root content changed"
        expect fileSystem.loadDirectory(f("/")).names, hasItem("dir/")

        and: "file is accessible by the new name"
        fileSystem.exists(f('/dir/file.txt'))
    }

    def "we can rename a file into the same directory"() {
        when: "rename file"
        fileSystem.rename(f("/dir/file.txt"), f("/dir/file_with_new_name.txt"))

        then: "file name changed"
        fileSystem.loadDirectory(f("/dir/")).names == ['file_with_new_name.txt'] as Set
    }

    def "we cannot move directory to itself"() {
        when: "rename file"
        fileSystem.rename(f("/dir/"), f("/dir/dir/"))

        then: "file name changed"
        thrown(IllegalArgumentException)
    }

    def "export root directory"() {
        given: "a temp directory"
        def tempDir = new File(System.getProperty("java.io.tmpdir") + "/test-temp-dir")

        when: "export content"
        fileSystem.exportFile(new VFile("/"), tempDir)

        then: "outer file system has the files copies"
        verifyAll {
            tempDir.exists()
            new File(tempDir, "a_new_file.txt").exists()
            new File(tempDir, "dir/file_with_new_name.txt").exists()
        }

        cleanup: "remove temp directory"
        FileUtils.deleteDirectory(tempDir)
    }

    def "we can move a file into another directory"() {
        when: "rename file"
        fileSystem.rename(f("/dir/file_with_new_name.txt"), f("/root_file.txt"))

        then: "file name changed"
        fileSystem.loadDirectory(f("/dir/")).isEmpty()
        fileSystem.loadDirectory(f("/")).names == ['dir/', 'a_new_file.txt', 'root_file.txt'] as Set
    }

    def "getStatistics() returns a correct data"() {
        when: "request a stat"
        def statistics = fileSystem.getStatistics()

        then: "correct info returns"

        verifyAll {
            statistics.directoriesCount == 2
            statistics.filesCount == 2
            statistics.usedBlocksCount == 6
            statistics.emptyBlocksCount == 1
            statistics.lostSpaceCount == 0
            statistics.totalBlocksCount == 7
        }
    }

    def "import files from OS"() {
        when: "import files"
        def directory = fileSystem.createDirectory(new VFile("/"), "imported");
        fileSystem.importFile(new File("src/test/resources"), directory)

        def imported = fileSystem.loadDirectory(new VFile("/imported/resources/"))

        then: "has imported content"
        imported.names == ['logback.xml'] as Set
    }

    def "a directory becomes empty if delete all files"() {
        when: 'delete all files'
        fileSystem.remove(f('/root_file.txt'))
        fileSystem.remove(f('/a_new_file.txt'))
        fileSystem.remove(f('/dir/'))
        fileSystem.remove(f('/imported/'))

        then: "the directory is empty"
        fileSystem.loadDirectory(f('/')).getNames().isEmpty()

        when: "try to read the deleted file"
        fileSystem.readFromFile(f('/root_file.txt'))

        then: "exception is thrown"
        thrown(FileNotFoundException)

        when: "get statistic"
        def stat = fileSystem.statistics

        then: "stat is empty"
        verifyAll {
            stat.usedBlocksCount == 2 // root directory content + initial block
            stat.emptyBlocksCount > 0
            stat.filesCount == 0
            stat.directoriesCount == 1 // root directory
            stat.maxDepth == 0
        }

        and: "no lost blocks"
        stat.lostSpaceCount == 0
    }

    def "empty space is reused when create files again"() {
        given: "statistic contains empty blocks"
        def oldStatistics = fileSystem.statistics
        assert oldStatistics.emptyBlocksCount > 0

        when: "created a file"
        fileSystem.writeFile(new VFile("/file.txt"), IOUtils.toInputStream("content"))
        def newStatistic = fileSystem.statistics

        then: "content is reused"
        oldStatistics.totalBlocksCount == newStatistic.totalBlocksCount
        oldStatistics.emptyBlocksCount > newStatistic.emptyBlocksCount
    }

    static FileSystem createFileSystem() {
        FileSystemFactory.createFileSystem(createTempFile())
    }

    static File createTempFile() {
        def file = File.createTempFile('virtualfs-', '.vfs')
        file.deleteOnExit()
        file
    }

    private static VFile f(String fullName) {
        new VFile(fullName)
    }

}
