package com.jetbrains.virtualfs.files

import spock.lang.Specification

/**
 *
 *
 * Date: 26.05.2018
 * @author OneHalf
 */
class VFileTest extends Specification {

    def "should return back the name used for object creation"() {
        given:
        def file = new VFile("/directory/path/file.txt")

        expect:
        !file.isDirectory()
        file.getName() == "/directory/path/file.txt"
    }

    def "should return the short name without slashes when object represent a file"() {
        given:
        VFile file = new VFile("/directory/path/file.txt")

        expect:
        !file.isDirectory()
        file.getShortName() ==  "file.txt"
    }

    def "should return the short name of directory with slash at the end when object represent a directory"() {
        given:
        VFile file = new VFile("/directory/path/")

        expect:
        file.isDirectory()
        file.getShortName() ==  "path/"
    }

    def "should return parent directory name"() {
        given:
        VFile file = new VFile("/directory/path/")

        expect:
        file.getParent() == new VFile("/directory/")
    }

    def "should return root as parent"() {
        given:
        VFile file = new VFile("/directory/")

        expect:
        file.getParent() == new VFile("/")
    }

    def "should return null as parent when an object represent a root directory"() {
        given:
        VFile file = new VFile ("/")

        expect:
        file.isDirectory()
        file.getParent() == null
    }
}
