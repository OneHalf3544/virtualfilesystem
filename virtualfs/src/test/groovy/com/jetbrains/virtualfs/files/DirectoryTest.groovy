package com.jetbrains.virtualfs.files

import org.apache.commons.io.IOUtils
import spock.lang.Specification

import static com.jetbrains.virtualfs.blocks.Pointer.block

class DirectoryTest extends Specification {

    def "method toInputStream should serialize file names to stream"() {
        given: "directory with files"
        def pointer = block(0)
        def directory = new Directory(pointer, [
                "file1.txt": block(1),
                "file2.mp3": block(2)
        ]);

        when: "serialize directory"
        def inputStream = directory.toInputStream()
        def deserializedDirectory = Directory.fromInputStream(block(0), inputStream)

        then: "deserialized content is equal to original"
        deserializedDirectory != Directory.emptyDirectory(block(0))
        deserializedDirectory == directory
    }

    def "method toInputStream should stream with correct size"() {
        given: "directory with one file"
        def directory = new Directory(block(0), ["file with 30 chars in name.txt": block(1)]);

        when: "serialize directory to bytes"
        def bytes = IOUtils.toByteArray(directory.toInputStream())

        then: "deserialized content is equal to original"
        bytes.length == 30 + Integer.BYTES + Long.BYTES
    }
}
